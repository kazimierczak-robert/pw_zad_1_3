// Zad 1.3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdlib.h"
#include "time.h"
#include "omp.h"

void gnp(int n, float p, int **tab)
{
	float x;
	int i, j;
	
	for (i = 0; i < n - 1; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			x = rand() % 101;
			x = x / 100;
			if (x <= p)
			{
				tab[i][j] = 1;
				tab[j][i] = 1;
			}
		}
	}
}
int countEdges(int n, int** tab)
{
	int i, j, k = 0;
	for (i = 0; i < n - 1; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (tab[i][j])
			{
				k++;
			}
		}
	}
	return k;
}
int countEdges_wspolbiezny(int n, int** tab)
{
	int i, j, k = 0;
	#pragma omp parallel for shared(tab,n) private(i,j)
	for (i = 0; i < n - 1; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (tab[i][j])
			{
				#pragma omp critical
				k++;
			}
		}
	}
return k;
}
int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	int n = 6;
	int **tab = new int*[n];
	for (int i = 0; i < n; i++)
	{
		tab[i] = new int[n];
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			tab[i][j] = 0;
	}
	gnp(n, 0.5, tab);

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			printf("%d\t", tab[i][j]);
		}
		printf("\n");
	}

	printf("Algorytm sekwencyjny: %d\nAlgorytm wspolbiezny: %d\n", countEdges(n, tab), countEdges_wspolbiezny(n, tab));
	return 0;
}

